package ru.kuzin.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported!");
    }

    public CommandNotSupportedException(@Nullable final String argument) {
        super("Error! Command ``" + argument + "`` not supported!");
    }

}
