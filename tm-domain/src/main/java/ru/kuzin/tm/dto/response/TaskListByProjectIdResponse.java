package ru.kuzin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.model.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListByProjectIdResponse extends AbstractResponse {

    @NotNull
    private List<TaskDTO> tasks;

    public TaskListByProjectIdResponse(@NotNull final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}