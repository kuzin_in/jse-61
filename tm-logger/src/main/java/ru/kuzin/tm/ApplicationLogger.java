package ru.kuzin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kuzin.tm.component.Bootstrap;
import ru.kuzin.tm.configuration.LoggerConfiguration;

public final class ApplicationLogger {

    public static void main(@Nullable String... args) {
        @NotNull ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }

}