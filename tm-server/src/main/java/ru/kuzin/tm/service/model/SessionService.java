package ru.kuzin.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kuzin.tm.api.service.model.ISessionService;
import ru.kuzin.tm.exception.field.IdEmptyException;
import ru.kuzin.tm.exception.field.IndexIncorrectException;
import ru.kuzin.tm.exception.field.UserIdEmptyException;
import ru.kuzin.tm.exception.user.AuthenticationException;
import ru.kuzin.tm.model.Session;
import ru.kuzin.tm.repository.model.ISessionRepository;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Session> result;
        result = repository.findAllByUserId(userId);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean result;
        result = repository.existsById(id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session result;
        result = repository.getOneByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable Session result;
        @Nullable final List<Session> sessionList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        sessionList = repository.getOneByIndexAndUserId(userId, pageable);
        if (sessionList == null || sessionList.isEmpty()) return null;
        result = sessionList.get(0);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable Session session;
        @Nullable final List<Session> sessionList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        sessionList = repository.getOneByIndexAndUserId(userId, pageable);
        if (sessionList == null || sessionList.isEmpty()) return;
        session = sessionList.get(0);
        repository.deleteByUserIdAndId(userId, session.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@NotNull Session session) {
        if (session.getUser() == null) throw new UserIdEmptyException();
        repository.deleteByUserIdAndId(session.getUser().getId(), session.getId());
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Session session) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new AuthenticationException();
        if (session.getUser() == null || session.getUser().getId().isEmpty()) throw new UserIdEmptyException();
        repository.save(session);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Session add(@NotNull final Session session) {
        if (session.getUser() == null) throw new UserIdEmptyException();
        if (session.getUser().getId().isEmpty()) throw new UserIdEmptyException();
        repository.save(session);
        return session;
    }

}