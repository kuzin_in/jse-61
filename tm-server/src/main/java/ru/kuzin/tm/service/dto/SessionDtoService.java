package ru.kuzin.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kuzin.tm.api.service.dto.ISessionDtoService;
import ru.kuzin.tm.dto.model.SessionDTO;
import ru.kuzin.tm.exception.field.IdEmptyException;
import ru.kuzin.tm.exception.field.IndexIncorrectException;
import ru.kuzin.tm.exception.field.UserIdEmptyException;
import ru.kuzin.tm.exception.user.AuthenticationException;
import ru.kuzin.tm.repository.dto.ISessionDtoRepository;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class SessionDtoService implements ISessionDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Autowired
    private ISessionDtoRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<SessionDTO> result;
        result = repository.findAllByUserId(userId);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean result;
        result = repository.existsById(id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO result;
        result = repository.getOneByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable SessionDTO result;
        @Nullable final List<SessionDTO> sessionList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        sessionList = repository.getOneByIndexAndUserId(userId, pageable);
        if (sessionList == null || sessionList.isEmpty()) return null;
        result = sessionList.get(0);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable SessionDTO session;
        @Nullable final List<SessionDTO> sessionList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        sessionList = repository.getOneByIndexAndUserId(userId, pageable);
        if (sessionList == null || sessionList.isEmpty()) return;
        session = sessionList.get(0);
        repository.deleteByUserIdAndId(userId, session.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@NotNull SessionDTO session) {
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserIdAndId(session.getUserId(), session.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final SessionDTO session) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new AuthenticationException();
        session.setUserId(userId);
        repository.save(session);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO add(@NotNull final SessionDTO session) {
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new UserIdEmptyException();
        repository.save(session);
        return session;
    }

}