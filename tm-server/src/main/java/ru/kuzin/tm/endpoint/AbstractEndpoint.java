package ru.kuzin.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.kuzin.tm.api.service.IAuthService;
import ru.kuzin.tm.dto.model.SessionDTO;
import ru.kuzin.tm.dto.request.AbstractUserRequest;
import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.exception.user.AccessDeniedException;

@Controller
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    private IAuthService authService;

    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final SessionDTO session = authService.validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return authService.validateToken(token);
    }

}