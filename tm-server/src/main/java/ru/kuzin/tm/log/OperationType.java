package ru.kuzin.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}