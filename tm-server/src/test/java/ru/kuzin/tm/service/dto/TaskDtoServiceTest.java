package ru.kuzin.tm.service.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kuzin.tm.api.service.dto.ITaskDtoService;
import ru.kuzin.tm.api.service.dto.IUserDtoService;
import ru.kuzin.tm.configuration.ServerConfiguration;
import ru.kuzin.tm.dto.model.TaskDTO;
import ru.kuzin.tm.enumerated.EntitySort;
import ru.kuzin.tm.exception.entity.TaskNotFoundException;
import ru.kuzin.tm.exception.field.*;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.migration.AbstractSchemeTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.kuzin.tm.constant.TaskConstant.*;

@Category(UnitCategory.class)
public class TaskDtoServiceTest extends AbstractSchemeTest {

    @NotNull
    private ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private ITaskDtoService taskService = context.getBean(ITaskDtoService.class);

    @NotNull
    private IUserDtoService userService = context.getBean(IUserDtoService.class);

    @NotNull
    private List<TaskDTO> taskList;

    @Nullable
    private static String USER_ID_1;

    @Nullable
    private static String USER_ID_2;

    private static long USER_ID_COUNTER = 0;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_ID_1 = userService.create("task_serv_usr_1_" + USER_ID_COUNTER, "1").getId();
        USER_ID_2 = userService.create("task_serv_usr_2_" + USER_ID_COUNTER, "1").getId();
        taskList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_1_" + i);
            task.setDescription("Description_1_" + i);
            task.setUserId(USER_ID_1);
            taskList.add(task);
            taskService.add(USER_ID_1, task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_2_" + i);
            task.setDescription("Description_2_" + i);
            task.setUserId(USER_ID_2);
            taskList.add(task);
            taskService.add(USER_ID_2, task);
        }
    }

    @After
    public void closeConnection() {
        taskService.clear(USER_ID_1);
        taskService.clear(USER_ID_2);
        userService.clear();
    }

    @Test
    public void testClearPositive() {
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.findAll(USER_ID_1).size());
        taskService.clear(USER_ID_1);
        Assert.assertEquals(0, taskService.findAll(USER_ID_1).size());
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.findAll(USER_ID_2).size());
        taskService.clear(USER_ID_2);
        Assert.assertEquals(0, taskService.findAll(USER_ID_2).size());
    }

    @Test
    public void testClearNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllPositive() {
        @NotNull List<TaskDTO> tasks = taskService.findAll(USER_ID_1);
        Assert.assertNotNull(tasks);
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_ID_2);
        Assert.assertNotNull(tasks);
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllSortNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(NULLABLE_USER_ID, CREATED_ENTITY_SORT));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(EMPTY_USER_ID, CREATED_ENTITY_SORT));
    }

    @Test
    public void testFindAllSortPositive() {
        List<TaskDTO> tasks = taskService.findAll(USER_ID_1, EntitySort.BY_NAME);
        Assert.assertNotNull(tasks);
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_ID_1, NAME_ENTITY_SORT);
        Assert.assertNotNull(tasks);
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_ID_1, STATUS_ENTITY_SORT);
        Assert.assertNotNull(tasks);
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_ID_2, NULLABLE_ENTITY_SORT);
        Assert.assertNotNull(tasks);
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
    }

    @Test
    public void testAddTaskNegative() {
        @NotNull final TaskDTO task = new TaskDTO();
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(NULLABLE_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(EMPTY_USER_ID, task));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.add(USER_ID_1, NULLABLE_TASK));
    }

    @Test
    public void testAddTaskPositive() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("TaskAddTest");
        task.setDescription("TaskAddTest desc");
        taskService.add(USER_ID_1, task);
        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.findAll(USER_ID_1).size());
        task.setId(UUID.randomUUID().toString());
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById(NULLABLE_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById(EMPTY_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(USER_ID_1, NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(USER_ID_1, EMPTY_TASK_ID));
    }

    @Test
    public void testExistsByIdPositive() {
        for (final TaskDTO task : taskList) {
            Assert.assertTrue(taskService.existsById(task.getUserId(), task.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(NULLABLE_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(EMPTY_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_ID_1, NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_ID_1, EMPTY_TASK_ID));
    }

    @Test
    public void testFindOneByIdPositive() {
        for (final TaskDTO task : taskList) {
            Assert.assertEquals(task.getId(), taskService.findOneById(task.getUserId(), task.getId()).getId());
        }
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneByIndex(NULLABLE_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(USER_ID_1, NULLABLE_INDEX));
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(taskList.get(i).getId(), taskService.findOneByIndex(USER_ID_1, i + 1).getId());
        }
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertEquals(taskList.get(i).getId(), taskService.findOneByIndex(USER_ID_2, i - 4).getId());
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(NULLABLE_USER_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(EMPTY_USER_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(USER_ID_1, NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(USER_ID_1, EMPTY_TASK_ID));
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final TaskDTO task : taskList) {
            taskService.removeById(task.getUserId(), task.getId());
            Assert.assertFalse(taskService.findAll(task.getUserId()).contains(task));
        }
        Assert.assertEquals(0, taskService.findAll(USER_ID_1).size());
        Assert.assertEquals(0, taskService.findAll(USER_ID_2).size());
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeByIndex(NULLABLE_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeByIndex(EMPTY_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(USER_ID_1, NULLABLE_INDEX));
    }

    @Test
    public void testCreateTaskNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(EMPTY_USER_ID, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID_1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID_1, "", null));
    }

    @Test
    public void testCreateTaskPositive() {
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.findAll(USER_ID_1).size());
        taskService.create(USER_ID_1, "PROJ", "PROJ_DESC");
        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.findAll(USER_ID_1).size());

        Assert.assertEquals(INIT_COUNT_TASKS, taskService.findAll(USER_ID_2).size());
        taskService.create(USER_ID_2, "PROJ", "PROJ_DESC");
        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.findAll(USER_ID_2).size());

        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.findAll(USER_ID_1).size());
        taskService.create(USER_ID_1, "PROJ_2", "");
        Assert.assertEquals(INIT_COUNT_TASKS + 2, taskService.findAll(USER_ID_1).size());

        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.findAll(USER_ID_2).size());
        taskService.create(USER_ID_2, "PROJ_2", "");
        Assert.assertEquals(INIT_COUNT_TASKS + 2, taskService.findAll(USER_ID_2).size());

        Assert.assertEquals(INIT_COUNT_TASKS + 2, taskService.findAll(USER_ID_1).size());
        taskService.create(USER_ID_1, "PROJ_3", null);
        Assert.assertEquals(INIT_COUNT_TASKS + 3, taskService.findAll(USER_ID_1).size());
    }

    @Test
    public void testChangeTaskStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(EMPTY_USER_ID, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(USER_ID_1, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(USER_ID_1, "", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.changeTaskStatusById(USER_ID_1, UUID.randomUUID().toString(), null));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusById(USER_ID_1, taskService.findOneByIndex(USER_ID_1, 1).getId(), null));
    }

    @Test
    public void testChangeTaskStatusByIdPositive() {
        for (final TaskDTO task : taskList) {
            taskService.changeTaskStatusById(task.getUserId(), task.getId(), IN_PROGRESS_STATUS);
            Assert.assertEquals(IN_PROGRESS_STATUS, taskService.findOneById(task.getUserId(), task.getId()).getStatus());
        }
    }

    @Test
    public void testChangeTaskStatusByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusByIndex(NULLABLE_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusByIndex(EMPTY_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(USER_ID_1, NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(USER_ID_1, -1, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(UUID.randomUUID().toString(), 0, null));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusByIndex(USER_ID_1, 1, null));
    }

    @Test
    public void testChangeTaskStatusByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            taskService.changeTaskStatusByIndex(USER_ID_1, 1, IN_PROGRESS_STATUS);
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(IN_PROGRESS_STATUS, taskService.findAll(USER_ID_1).get(i).getStatus());
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            taskService.changeTaskStatusByIndex(USER_ID_2, 1, IN_PROGRESS_STATUS);
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(IN_PROGRESS_STATUS, taskService.findAll(USER_ID_2).get(i).getStatus());
        }
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(USER_ID_1, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(USER_ID_1, "", null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(USER_ID_1, UUID.randomUUID().toString(), null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(USER_ID_1, UUID.randomUUID().toString(), "", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.updateById(USER_ID_1, UUID.randomUUID().toString(), "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIdPositive() {
        for (final TaskDTO task : taskList) {
            taskService.updateById(task.getUserId(), task.getId(), task.getName(), null);
            taskService.updateById(task.getUserId(), task.getId(), task.getName(), "");
            taskService.updateById(task.getUserId(), task.getId(), task.getName() + "_upd", task.getDescription() + "_upd");
            Assert.assertEquals(task.getId(), taskService.findOneById(task.getUserId(), task.getId()).getId());
            Assert.assertEquals(task.getName() + "_upd", taskService.findOneById(task.getUserId(), task.getId()).getName());
            Assert.assertEquals(task.getDescription() + "_upd", taskService.findOneById(task.getUserId(), task.getId()).getDescription());
        }
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(USER_ID_1, null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(USER_ID_1, -1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(USER_ID_1, 1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(USER_ID_1, 1, "", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.updateByIndex(UUID.randomUUID().toString(), 1, "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            TaskDTO task = taskList.get(i);
            taskService.updateByIndex(task.getUserId(), i + 1, task.getName(), null);
            taskService.updateByIndex(task.getUserId(), i + 1, task.getName(), "");
            taskService.updateByIndex(task.getUserId(), i + 1, task.getName() + "_upd", task.getDescription() + "_upd");
            task.setName(task.getName() + "_upd");
            Assert.assertNotNull(
                    taskService.findAll(task.getUserId()).stream()
                            .filter(m -> task.getUserId().equals(m.getUserId()))
                            .filter(m -> task.getName().equals(m.getName()))
                            .findFirst()
                            .orElse(null)
            );
        }
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            TaskDTO task = taskList.get(i);
            taskService.updateByIndex(task.getUserId(), i - 4, task.getName(), null);
            taskService.updateByIndex(task.getUserId(), i - 4, task.getName(), "");
            taskService.updateByIndex(task.getUserId(), i - 4, task.getName() + "_upd", task.getDescription() + "_upd");
            task.setName(task.getName() + "_upd");
            Assert.assertNotNull(
                    taskService.findAll(task.getUserId()).stream()
                            .filter(m -> task.getUserId().equals(m.getUserId()))
                            .filter(m -> task.getName().equals(m.getName()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testFindAllByProjectIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(NULLABLE_USER_ID, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(EMPTY_USER_ID, null));
    }

    @Test
    public void testFindAllByProjectIdPositive() throws Exception {
        Assert.assertEquals(Collections.emptyList(), taskService.findAllByProjectId(USER_ID_1, null));
        Assert.assertEquals(Collections.emptyList(), taskService.findAllByProjectId(USER_ID_1, ""));
        for (final TaskDTO task : taskList) {
            if (task.getProjectId() != null)
                Assert.assertNotNull(
                        taskService.findAllByProjectId(task.getUserId(), task.getProjectId())
                                .stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
    }

}